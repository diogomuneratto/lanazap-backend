import { AnyMessageContent, WALegacySocket, WAMessage } from "@adiwajshing/baileys";
import AppError from "../../errors/AppError";
import GetTicketWbot from "../../helpers/GetTicketWbot";
import Message from "../../models/Message";
import Ticket from "../../models/Ticket";

import formatBody from "../../helpers/Mustache";

interface Request {
  body: string;
  ticket: Ticket;
  quotedMsg?: Message;
  forwardedMsg?: Message;
}

const SendWhatsAppMessage = async ({
  body,
  ticket,
  quotedMsg,
  forwardedMsg
}: Request): Promise<WAMessage> => {
  let options = {};
  const wbot = await GetTicketWbot(ticket);
  const number = `${ticket.contact.number}@${
    ticket.isGroup ? "g.us" : "s.whatsapp.net"
  }`;
  if (quotedMsg) {
    if (wbot.type === "legacy") {
      const chatMessages = await (wbot as WALegacySocket).loadMessageFromWA(
        number,
        quotedMsg.id
      );

      options = {
        quoted: chatMessages
      };
    }

    if (wbot.type === "md") {
      const chatMessages = await Message.findOne({
        where: {
          id: quotedMsg.id
        }
      });

      const msgFound = JSON.parse(chatMessages.dataJson);

      options = {
        quoted: {
          key: msgFound.key,
          message: msgFound.message
        }
      };
    }
  }
  let content: AnyMessageContent
  if (forwardedMsg) {
    if (wbot.type === "legacy") {
      const chatMessages = await (wbot as WALegacySocket).loadMessageFromWA(
        number,
        forwardedMsg.id
      );
      content = {
        forward: chatMessages
      };
    }
    if (wbot.type === "md") {
      const chatMessages = await Message.findOne({
        where: {
          id: forwardedMsg.id
        }
      });
      const msgFound = JSON.parse(chatMessages.dataJson);
      content = {
        forward: msgFound
      };
    }
    try {
      const sentMessage = await wbot.sendMessage(
        number,
        {
          ...content
        },
        {
          ...options
        }
      );
      await ticket.update({ lastMessage: formatBody(forwardedMsg.body, ticket.contact) });
      return sentMessage;
    } catch (err) {
      throw new AppError("ERR_SENDING_WAPP_MSG");
    }
  } else {
    try {
      const sentMessage = await wbot.sendMessage(
        number,
        { 
          text: formatBody(body, ticket.contact)
        },
        {
          ...options
        }
      );
      await ticket.update({ lastMessage: formatBody(body, ticket.contact) });
      return sentMessage;
    } catch (err) {
      throw new AppError("ERR_SENDING_WAPP_MSG");
    }
  };
}

export default SendWhatsAppMessage;
