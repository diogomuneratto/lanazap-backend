import gracefulShutdown from "http-graceful-shutdown";
import https from "https";
import fs from "fs";
import swaggerUi from "swagger-ui-express";
import app from "./app";
import { initIO } from "./libs/socket";
import { logger } from "./utils/logger";
import { StartAllWhatsAppsSessions } from "./services/WbotServices/StartAllWhatsAppsSessions";

import swaggerDocs from "./swagger.json";

const options = {
  customCss: ".swagger-ui .topbar { display: none }"
};

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs, options));

let server;
if (process.env.NODE_ENV === "PRODUCTION") {
  const serverConfig = {
    key: fs.readFileSync(process.env.PRIVKEY),
    cert: fs.readFileSync(process.env.CERT)
  };
  server = https
    .createServer(serverConfig, app)
    .listen(process.env.PORT, () => {
      logger.info(`Servidor https iniciado na porta: ${process.env.PORT}`);
    });
} else {
  server = app.listen(process.env.PORT, () => {
    logger.info(`Servidor iniciado na porta: ${process.env.PORT}`);
  });
}

initIO(server);
StartAllWhatsAppsSessions();
gracefulShutdown(server);
